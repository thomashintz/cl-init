(defpackage #:cl-init
  (:use #:cl)
  (:export #:*swank-port* #:*start-thunk* #:*stop-thunk* #:run))

