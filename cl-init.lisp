(in-package #:cl-init)

(defparameter *start-thunk* #'(lambda () nil))
(defparameter *stop-thunk* #'(lambda () nil))

(defparameter *shutdown-port* 6200)

(require 'swank)
(defparameter *swank-port* 4006)
(defparameter *swank-server* nil)

(defun run ()
  (format t "~A ~S~%" "starting swank on port" *swank-port*)
  (setf *swank-server*
        (swank:create-server :port *swank-port* :dont-close t))
  (format t "swank started~%starting external process~%")

  (apply *start-thunk* nil)
  (format t "external process running~%")

  (ccl:with-open-socket (socket :connect :passive
                                :format :text
                                :local-port *shutdown-port*
                                :reuse-address t)
    (with-open-stream (stream (ccl:accept-connection socket))
      (format t "stopping external process~%")
      (apply *stop-thunk* nil)
      (format t "external process stopped~%stopping swank~%")
      (swank:stop-server *swank-port*)
      (format t "swank stopped~%")
      (princ "server shutdown complete" stream)))

  (ccl:quit))
